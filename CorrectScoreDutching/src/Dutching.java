import java.util.Scanner;  

public class Dutching {

	public static void main(String[] args) {
	
		 Scanner sc= new Scanner(System.in);  
		 
		 // Introduction - Total Stake and What Teams you are choosing to dutch
		 
		 System.out.println("Enter Total Stake in euro");  
		 double totalStake=sc.nextDouble();
		 
		 System.out.println("Enter the First Team you are Betting on ");  
		 String team1=sc.next(); 
		 
		 System.out.println("Enter the Second Team you are Betting on ");  
		 String team2=sc.next();  
		 
		 
		 
		 // First Correct Score Selection
		 
		   System.out.println("GAME 1 : Enter Correct Scores for "+team1);  
		 	
		   System.out.println("Enter the Correct Score ");  
		   String GameOneS1=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameOneS1=sc.nextDouble();
		   
		// Second Correct Score Selection
			 
		   System.out.println("Enter the Correct Score ");  
		   String GameOneS2=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameOneS2=sc.nextDouble();
		   
		// Third Correct Score Selection
			 
		   System.out.println("Enter the Correct Score ");  
		   String GameOneS3=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameOneS3=sc.nextDouble();
		   
		// Fourth Correct Score Selection
			 
		   System.out.println("Enter the Correct Score ");  
		   String GameOneS4=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameOneS4=sc.nextDouble();
		   
		   
		   //**** GAME 2 SELECTIONS NEXT ****
		   
		   
			 // First Correct Score Selection
			 
		   System.out.println("GAME 2 : Enter Correct Scores for "+team2); 
		   
		   
		   System.out.println("Enter the Correct Score ");  
		   String GameTwoS1=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameTwoS1=sc.nextDouble();
		   
		// Second Correct Score Selection
			 
		   System.out.println("Enter the Correct Score ");  
		   String GameTwoS2=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameTwoS2=sc.nextDouble();
		   
		// Third Correct Score Selection
			 
		   System.out.println("Enter the Correct Score ");  
		   String GameTwoS3=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameTwoS3=sc.nextDouble();
		   
		// Fourth Correct Score Selection
			 
		   System.out.println("Enter the Correct Score ");  
		   String GameTwoS4=sc.next();  
		   System.out.println("Enter the Correct Score Odds");  
		   double oddsGameTwoS4=sc.nextDouble();
		
		   
		// ** CALCULATION PROCESS **   
		   
		   double totalOdds1 = oddsGameOneS1 * oddsGameTwoS1;
		   double totalOdds2 = oddsGameOneS1 * oddsGameTwoS2;
		   double totalOdds3 = oddsGameOneS1 * oddsGameTwoS3;
		   double totalOdds4 = oddsGameOneS1 * oddsGameTwoS4;
		   
		   double totalOdds5 = oddsGameOneS2 * oddsGameTwoS1;
		   double totalOdds6 = oddsGameOneS2 * oddsGameTwoS2;
		   double totalOdds7 = oddsGameOneS2 * oddsGameTwoS3;
		   double totalOdds8 = oddsGameOneS2 * oddsGameTwoS4;
		   
		   double totalOdds9 = oddsGameOneS3 * oddsGameTwoS1;
		   double totalOdds10 = oddsGameOneS3 * oddsGameTwoS2;
		   double totalOdds11 = oddsGameOneS3 * oddsGameTwoS3;
		   double totalOdds12 = oddsGameOneS3 * oddsGameTwoS4;
		   
		   double totalOdds13 = oddsGameOneS4 * oddsGameTwoS1;
		   double totalOdds14 = oddsGameOneS4 * oddsGameTwoS2;
		   double totalOdds15 = oddsGameOneS4 * oddsGameTwoS3;
		   double totalOdds16 = oddsGameOneS4 * oddsGameTwoS4;
		   
		   // *** Stakes ***
		   
		   //Use implied probability
		   
		   double implied1 = (1 / (totalOdds1 + 1)) * 100;
		   double implied2 = (1 / (totalOdds2 + 1)) * 100;
		   double implied3 = (1 / (totalOdds3 + 1)) * 100;
		   double implied4 = (1 / (totalOdds4 + 1)) * 100;
		   double implied5 = (1 / (totalOdds5 + 1)) * 100;
		   double implied6 = (1 / (totalOdds6 + 1)) * 100;
		   double implied7 = (1 / (totalOdds7 + 1)) * 100;
		   double implied8 = (1 / (totalOdds8 + 1)) * 100;
		   double implied9 = (1 / (totalOdds9 + 1)) * 100;
		   double implied10 = (1 / (totalOdds10 + 1)) * 100;
		   double implied11 = (1 / (totalOdds11 + 1)) * 100;
		   double implied12 = (1 / (totalOdds12 + 1)) * 100;
		   double implied13 = (1 / (totalOdds13 + 1)) * 100;
		   double implied14 = (1 / (totalOdds14 + 1)) * 100;
		   double implied15 = (1 / (totalOdds15 + 1)) * 100;
		   double implied16 = (1 / (totalOdds16 + 1)) * 100;
		   
		   
		   //Working out stakes
		   
		   double stake1 = (implied1 / (implied2 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied1 + implied16)) * totalStake;
		   double stake2 = (implied2 / (implied1 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied2 + implied16)) * totalStake;
		   double stake3 = (implied3 / (implied1 + implied2 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied3 + implied16)) * totalStake;
		   double stake4 = (implied4 / (implied1 + implied2 + implied3 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied4 + implied16)) * totalStake;
		   double stake5 = (implied5 / (implied1 + implied2 +implied3 + implied4 + implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied5 + implied16)) * totalStake;
		   double stake6 = (implied6 / (implied1 + implied2 + implied3 + implied4 + implied5+  implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied6 + implied16)) * totalStake;
		   double stake7 = (implied7 / (implied1 + implied2 +implied3 + implied4 + implied5+ implied6+  implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied7 + implied16)) * totalStake;
		   double stake8 = (implied8 / (implied1 + implied2 + implied3 + implied4 + implied5+ implied6+ implied7+  implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied8 + implied16)) * totalStake;
		   double stake9 = (implied9 / (implied1 + implied2 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+  implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied9 + implied16)) * totalStake;
		   double stake10 = (implied10 / (implied1 + implied2 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+  implied11+ implied12 + implied13+ implied14+ implied15 + implied10 + implied16)) * totalStake;
		   double stake11 = (implied11 / (implied1 + implied2 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+  implied12 + implied13+ implied14+ implied15 + implied11 + implied16)) * totalStake;
		   double stake12 = (implied12 / (implied1 +implied2 +  implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+  implied13+ implied14+ implied15 + implied12 + implied16)) * totalStake;
		   double stake13 = (implied13 / (implied1 +implied2 +  implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 +  implied14+ implied15 + implied13 + implied16)) * totalStake;
		   double stake14 = (implied14 / (implied1 + implied2 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+  implied15 + implied14 + implied16)) * totalStake;
		   double stake15 = (implied15 / (implied1 + implied2 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14  + implied15 + implied16)) * totalStake;
		   double stake16 = (implied16 / (implied1 + implied2 + implied3 + implied4 + implied5+ implied6+ implied7+ implied8+ implied9+ implied10+ implied11+ implied12 + implied13+ implied14+ implied15 + implied16)) * totalStake;
		   
		   //Working out Precise Returns for each selection
		   
		   double returns1 = stake1*totalOdds1;
		   double returns2 = stake2*totalOdds2;
		   double returns3 = stake3*totalOdds3;
		   double returns4 = stake4*totalOdds4;
		   double returns5 = stake5*totalOdds5;
		   double returns6 = stake6*totalOdds6;
		   double returns7 = stake7*totalOdds7;
		   double returns8 = stake8*totalOdds8;
		   double returns9 = stake9*totalOdds9;
		   double returns10 = stake10*totalOdds10;
		   double returns11 = stake11*totalOdds11;
		   double returns12 = stake12*totalOdds12;
		   double returns13 = stake13*totalOdds13;
		   double returns14 = stake14*totalOdds14;
		   double returns15 = stake15*totalOdds15;
		   double returns16 = stake16*totalOdds16;
		   
		   
		   //Printing Info
		   
		   System.out.print("1. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS1);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS1);
		   System.out.printf(" is : %.2f , returning: ", stake1);
		   System.out.printf("%.2f",returns1);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   
		   System.out.print("2. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS1);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS2);
		   System.out.printf(" is : %.2f , returning: ", stake2);
		   System.out.printf("%.2f",returns2);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("3. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS1);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS3);
		   System.out.printf(" is : %.2f , returning: ", stake3);
		   System.out.printf("%.2f",returns3);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("4. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS1);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS4);
		   System.out.printf(" is : %.2f , returning: ", stake4);
		   System.out.printf("%.2f",returns4);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("5. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS2);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS1);
		   System.out.printf(" is : %.2f , returning: ", stake5);
		   System.out.printf("%.2f",returns5);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("6. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS2);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS2);
		   System.out.printf(" is : %.2f , returning: ", stake6);
		   System.out.printf("%.2f",returns6);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("7. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS2);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS3);
		   System.out.printf(" is : %.2f , returning: ", stake7);
		   System.out.printf("%.2f",returns7);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("8. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS2);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS4);
		   System.out.printf(" is : %.2f , returning: ", stake8);
		   System.out.printf("%.2f",returns8);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("9. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS3);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS1);
		   System.out.printf(" is : %.2f , returning: ", stake9);
		   System.out.printf("%.2f",returns9);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("10. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS3);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS2);
		   System.out.printf(" is : %.2f , returning: ", stake10);
		   System.out.printf("%.2f",returns10);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("11. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS3);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS3);
		   System.out.printf(" is : %.2f , returning: ", stake11);
		   System.out.printf("%.2f",returns11);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("12. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS3);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS4);
		   System.out.printf(" is : %.2f , returning: ", stake12);
		   System.out.printf("%.2f",returns12);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("13. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS4);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS1);
		   System.out.printf(" is : %.2f , returning: ", stake13);
		   System.out.printf("%.2f",returns13);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("14. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS4);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS2);
		   System.out.printf(" is : %.2f , returning: ", stake14);
		   System.out.printf("%.2f",returns14);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("15. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS4);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS3);
		   System.out.printf(" is : %.2f , returning: ", stake15);
		   System.out.printf("%.2f",returns15);
		   System.out.print(" if any selection wins. Good Luck! ");
		   
		   System.out.println(" ");
		   
		   System.out.print("16. The Stake for "+team1);
		   System.out.print(" ");
		   System.out.print(GameOneS4);
		   System.out.print(" and "+team2);
		   System.out.print(" ");
		   System.out.print(GameTwoS4);
		   System.out.printf(" is : %.2f , returning: ", stake16);
		   System.out.printf("%.2f",returns16);
		   System.out.print(" if any selection wins. Good Luck! ");
				   
	}

}
